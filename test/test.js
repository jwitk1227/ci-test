var request = require('supertest');
var app = require('../app.js');

describe('GET /', () => {
    it('displays "Hello World!"', done => {
        request(app).get('/').expect('Hello World!', done);
    }); 
});